#!/bin/bash

#######################################################
#
#  Description: Deploiment de container Docker
#
#   Auteur: Youssef
#
#   Date : 08/06/2024
########################################################

## si option --create
if [ "$1" == "--create" ];then
    
    # définition du  nombre de machine par défaut 1 machine 
    nb_machine=1
    [ "$2" != "" ] && nb_machine=$2
    
    ## Setting min et max
    min=1
    max=0

    #### Récupération de L'ID Max

    idmax=`docker ps -a --format '{{ .Names}}' | awk -F "-" -v user="$USER" '$0 ~ user"-alpine" {print $3}' | sort -r |head -1`
     

    ## Redéfinition de min max
    min=$(($idmax + 1))
    max=$(($idmax + $nb_machine))

    # creation des containers
    ## Début création des Containers
    for i in $(seq $min $max);do
        docker run -tid --name $USER-alpine-$i alpine:latest
        echo "Container $USER-alpine-$i créé"
    done

    echo "j'ai créé $nb_machine machine"

## si option --drop
elif [ "$1" == "--drop" ];then
    echo "suppression des containers"
    docker rm -f $(docker ps -a | grep $USER-alpine | awk '{print $1}')
    echo "fin de suppression des containers"

    #docker rm -f $USER-alpine

## si option --infos
elif [ "$1" == "--infos" ];then
    echo "Informations sur les containers - afficher les information Nom et IP"
    for container in $(docker ps -a | grep $USER-alpine | awk '{print $1}');do
        docker inspect -f ' ==> {{ .Name }} - {{.NetworkSettings.IPAddress}}' $container
    done
    echo ""

## si option --start
elif [ "$1" == "--start" ];then
    echo ""
    docker start $(docker ps -a | grep $USER-alpine | awk '{print $1}')
    echo ""

## si option --ansible
elif [ "$1" == "--ansible" ];then
    echo ""
    echo "notre options est --ansible"
    echo ""


else


echo "

Options: 

        - --create  : lancer des containers

        - --drop    : supprimer les containers créer par le scripts

        - --infos   : caractéristiques des containers (ip, nom , user ..)

        - --start:  : démarrer les containers

        - --ansible : deploiment arborscence Ansible

"

fi